import {useContext, DragEvent} from 'react';
import {
  Box,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  IconButton,
  Typography,
} from '@mui/material';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import ModeEditOutlineIcon from '@mui/icons-material/ModeEditOutline';
import {Entry} from '../../interfaces';
import {UIContext} from '../../context/ui';
import {EntriesContext} from '../../context/entries';
import {useRouter} from 'next/router';
import {timeSince} from '../../services';

interface Props {
    entry: Entry
}


export const EntryCard: React.FC<Props> = ({entry}) => {
  const {deleteEntry} = useContext(EntriesContext);
  const {setIsDragging} = useContext( UIContext );
  const router = useRouter();

  const onDragStart = (event: DragEvent) => {
    event.dataTransfer.setData('text', entry._id);
    setIsDragging(true);
  };

  const onDragEnd = () => {
    setIsDragging(false);
  };

  const onClick = () => {
    router.push(`/entries/${entry._id}`);
  };

  return (
    <Card
      sx={{marginBottom: 1}}
      draggable
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
    >
      <CardActionArea>
        <CardContent>
          <Box
            sx={{position: 'absolute', top: 10, right: 10}}
          >
            <IconButton
              size='large'
              edge='start'
              onClick={onClick}>
              <ModeEditOutlineIcon fontSize='small'
              />
            </IconButton>

            <IconButton
              size='large'
              edge='start'
              onClick={() => deleteEntry(entry)}>
              <DeleteOutlineIcon fontSize='small'
              />
            </IconButton>
          </Box>

          <Typography sx={{whiteSpace: 'pre-line'}}>
            {entry.description}
          </Typography>
        </CardContent>
        <CardActions
          sx={{display: 'flex', justifyContent: 'end', paddingRight: 2}}
        >
          <Typography variant='body2'>
            {timeSince(entry.createdAt)} ago
          </Typography>
        </CardActions>
      </CardActionArea>
    </Card>
  );
};
