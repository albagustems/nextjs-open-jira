import {Button, Box, TextField} from '@mui/material';
import SaveOutlinedIcon from '@mui/icons-material/SaveOutlined';
import AddCircleOutlinedOutlinedIcon
  from '@mui/icons-material/AddCircleOutlineOutlined';
import {useState, useContext} from 'react';
import {EntriesContext} from '../../context/entries';
import {UIContext} from '../../context/ui';

export const NewEntry = () => {
  const [inputValue, setInputValue] = useState('');
  const [isTouched, setIsTouched] = useState(false);

  const {addNewEntry} = useContext(EntriesContext);
  const {setIsAddingEntry, isAddingEntry} = useContext(UIContext);

  const onTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const onCancel = () => {
    setIsAddingEntry(false);
    setIsTouched(false);
    setInputValue('');
  };

  const onSave = () => {
    if (inputValue.length <= 0) return;
    addNewEntry(inputValue);
    onCancel();
  };

  return (
    <Box sx={{marginBottom: 2, paddingX: 2}}>
      {isAddingEntry && (
        <>
          <TextField
            sx={{marginTop: 2, marginBottom: 1}}
            placeholder='New entry'
            label='New entry'
            helperText='Add a value'
            error={inputValue.length <= 0 && isTouched}
            value={inputValue}
            onChange={onTextChange}
            onBlur={() => setIsTouched(true)}
            fullWidth
            autoFocus
            multiline
          />

          <Box display='flex' justifyContent='space-between'>
            <Button
              onClick={onCancel}
              variant='text'
              color='secondary'>
            Cancel
            </Button>
            <Button
              variant='outlined'
              color='secondary'
              onClick={onSave}
              endIcon={ <SaveOutlinedIcon/> }>
            Save
            </Button>
          </Box>
        </>
      )}

      {!isAddingEntry && (
        <Button
          startIcon={<AddCircleOutlinedOutlinedIcon />}
          onClick={() => setIsAddingEntry(true)}
          variant='outlined'
          fullWidth
        >
          Add new task
        </Button>
      )}
    </Box>
  );
};
