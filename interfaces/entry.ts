export type EntryStatus = 'to-do' | 'in-progress' | 'under-review' | 'done';

export interface Entry {
    _id: string;
    description: string;
    createdAt: number;
    status: EntryStatus;
}
