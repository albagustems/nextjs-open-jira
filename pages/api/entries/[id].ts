import type {NextApiRequest, NextApiResponse} from 'next';
import {db} from '../../../database';
import {Entry, EntryInterface} from '../../../models';

type Data =
    | { message: string }
    | EntryInterface[]
    | EntryInterface

export default function handler(
    req: NextApiRequest, res: NextApiResponse<Data>,
) {
  const {id} = req.query;
  switch (req.method) {
    case 'GET':
      return getEntry(id!.toString(), res);
    case 'PUT':
      return updateEntry(req, res);
    case 'DELETE':
      return deleteEntry(id!.toString(), res);
    default:
      return res.status(400).json({message: 'Bad request'});
  }
}

const getEntry = async (id: string, res: NextApiResponse<Data>) => {
  await db.connect();
  const entry = await Entry.findOne({_id: id});
  await db.disconnect();

  if (!entry) {
    return res.status(400).json({message: 'Entry not found'});
  }

  return res.status(200).json(entry);
};

const updateEntry = async (req: NextApiRequest, res: NextApiResponse<Data>) => {
  const {id} = req.query;

  await db.connect();
  const entry = await Entry.findOne({_id: id});

  if (!entry) {
    await db.disconnect();
    return res.status(400).json({message: 'Entry not found'});
  }

  try {
    const {description = entry.description, status = entry.status} = req.body;
    const updatedEntry = await Entry.findOneAndUpdate(
        {_id: id}, {description, status}, {runValidators: true, new: true},
    );

    db.disconnect();
    return res.status(200).json(updatedEntry!);
  } catch (error: unknown) {
    const {message} = error as Error;
    await db.disconnect();

    return res.status(500).json({message: message});
  }
};

const deleteEntry = async (id: string, res: NextApiResponse<Data>) => {
  try {
    await db.connect();
    await Entry.findOneAndDelete({_id: id});
    await db.disconnect();

    return res.status(200).json({message: 'Entry deleted'});
  } catch (error: unknown) {
    const {message} = error as Error;
    await db.disconnect();

    return res.status(400).json({message});
  }
};
