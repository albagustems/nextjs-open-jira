import {useState, useMemo, useContext} from 'react';
import {GetServerSideProps} from 'next';
import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  TextField,
  CardActions,
  Button,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  capitalize,
  IconButton,
} from '@mui/material';
import SaveOutlinedIcon from '@mui/icons-material/SaveOutlined';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import {Layout} from '../../components/layouts';
import {Entry, EntryStatus} from '../../interfaces';
import {dbEntries} from '../../database';
import {timeSince} from '../../services';
import {EntriesContext} from '../../context/entries';
import {useRouter} from 'next/router';

const validStatus: EntryStatus[] = [
  'to-do',
  'in-progress',
  'under-review',
  'done',
];

interface Props {
    entry: Entry
}

export const EntyPage: React.FC<Props> = ({entry}) => {
  const [inputValue, setInputValue] = useState(entry.description);
  const [status, setStatus] = useState<EntryStatus>(entry.status);
  const [isTouched, setIsTouched] = useState(false);

  const {updateEntry, deleteEntry} = useContext(EntriesContext);
  const router = useRouter();

  const isNotValid = useMemo(() =>
    inputValue.length <=0 && isTouched,
  [inputValue, isTouched],
  );

  const onTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const onStatusChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setStatus(e.target.value as EntryStatus);
  };

  const onSave = () => {
    if (inputValue.trim().length <= 0) return;

    const updatedEntry: Entry = {
      ...entry,
      status,
      description: inputValue,
    };

    updateEntry(updatedEntry, true);
    router.replace('/');
  };

  const onDelete = () => {
    deleteEntry(entry, true);
    router.replace('/');
  };

  return (
    <Layout title={inputValue.substring(0, 20) + '...'}>
      <Grid
        container
        justifyContent='center'
        sx={{marginTop: 2}}
      >
        <Grid item xs={ 12 } sm={ 8 } md={ 6 }>
          <Card>
            <CardHeader
              title='Entry'
              subheader={`Created ${timeSince(entry.createdAt)} ago`} />
            <CardContent>
              <TextField
                sx={{marginTop: 2, marginBottom: 1}}
                fullWidth
                placeholder='New entry'
                autoFocus
                multiline
                label='New entry'
                value={inputValue}
                onChange={onTextChange}
                onBlur={() => setIsTouched(true)}
                helperText={isNotValid && 'Insert a value'}
                error={isNotValid}
              />
              <FormControl>
                <FormLabel>
                    Status
                </FormLabel>
                <RadioGroup
                  row
                  value={status}
                  onChange={onStatusChange}
                >
                  {
                    validStatus.map((option) => (
                      <FormControlLabel
                        key={option}
                        value={option}
                        control={<Radio/>}
                        label={capitalize(option)}
                      />
                    ))
                  }
                </RadioGroup>
              </FormControl>
            </CardContent>
            <CardActions>
              <Button
                startIcon={<SaveOutlinedIcon/>}
                variant='contained'
                fullWidth
                onClick={onSave}
                disabled={inputValue.length <=0}
              >
                Save
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
      <IconButton
        onClick={onDelete}
        sx={{
          'position': 'fixed',
          'bottom': 30,
          'right': 30,
          'backgroundColor': 'error.dark',
          '&:hover': {
            backgroundColor: 'error.light',
          },
        }}>
        <DeleteOutlineIcon/>
      </IconButton>
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({params}) => {
  const {id} = params as { id: string};
  const entry = await dbEntries.getEntryById(id);

  if (!entry) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    };
  }

  return {
    props: {
      entry,
    },
  };
};

export default EntyPage;
