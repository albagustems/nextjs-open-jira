import {Grid, Card, CardHeader} from '@mui/material';
import type {NextPage} from 'next';
import {Layout} from '../components/layouts';
import {EntryList, NewEntry} from '../components/ui';

const Home: NextPage = () => {
  return (
    <Layout title='Home - OpenJira'>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={3}>
          <Card sx={{height: 'calc(100vh - 100px)'}}>
            <CardHeader title="To Do" />
            <NewEntry />
            <EntryList status='to-do' />
          </Card>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Card sx={{height: 'calc(100vh - 100px)'}}>
            <CardHeader title="In Progress" />
            <EntryList status='in-progress'/>
          </Card>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Card sx={{height: 'calc(100vh - 100px)'}}>
            <CardHeader title="Under Review" />
            <EntryList status='under-review' />
          </Card>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Card sx={{height: 'calc(100vh - 100px)'}}>
            <CardHeader title="Done" />
            <EntryList status='done' />
          </Card>
        </Grid>
      </Grid>

    </Layout>
  );
};

export default Home;
