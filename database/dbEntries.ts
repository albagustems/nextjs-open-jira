import {Entry, EntryInterface} from '../models';
import {db} from './';

export const getEntryById =
    async (id: string): Promise<EntryInterface | null> => {
      await db.connect();
      const entry = await Entry.findOne({_id: id}).lean();
      await db.disconnect();
      return entry;
    };
