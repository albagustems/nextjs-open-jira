interface SeedData {
    entries: SeedEntry[]
}

interface SeedEntry {
    description: string;
    status: string;
    createdAt: number;
}

export const seedData: SeedData = {
  entries: [
    {
      description: 'To Do: Example 1',
      status: 'to-do',
      createdAt: Date.now(),
    },
    {
      description: 'In Progress: Example 2',
      status: 'in-progress',
      createdAt: Date.now() - 1000000,
    },
    {
      description: 'Under Review: Example 3',
      status: 'under-review',
      createdAt: Date.now(),
    },
    {
      description: 'Done: Example 4',
      status: 'done',
      createdAt: Date.now() -100000,
    },
  ],
};
