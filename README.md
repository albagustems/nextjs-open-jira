This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started
We need a concrete node version to run the project
```
16.0.0
```

Configure the environment keys:
Rename the file __.env.example__ to __.env__ and fill the necesary keys
To configure the local database you will need to fill the MONGO_URL variable.The Mongo DB local URL should be: __mongodb://localhost:27017/entriesdb__

To be able to run de app, we need the database 
```
docker-compose up -d
```

Install the dependencies:
```bash
npm install
# or
yarn install
```

To run the development server:

```bash
npm run dev
# or
yarn dev
```



If you need data to fill the database, you can use the endpoint 
```
http://localhost:3000/api/seed
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
