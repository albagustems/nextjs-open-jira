import {useReducer} from 'react';
import {UIContext, UIReducer} from './';

export interface UIState {
    sideMenuOpen: boolean;
    isAddingEntry: boolean;
    isDragging: boolean;
    children?: React.ReactNode;
    openSideMenu: () => void;
    closeSideMenu: () => void;
    setIsAddingEntry: (bool: boolean) => void;
    setIsDragging: (bool: boolean) => void;
}


const UI_INITIAL_STATE: UIState = {
  sideMenuOpen: false,
  isAddingEntry: false,
  isDragging: false,
  openSideMenu: () => {},
  closeSideMenu: () => {},
  setIsAddingEntry: (bool: boolean) => {},
  setIsDragging: (bool: boolean) => {},
};

type Props = {
  children: React.ReactNode
}

export const UIProvider:React.FC<Props> = ({children}) => {
  const [state, dispatch] = useReducer(UIReducer, UI_INITIAL_STATE);

  const openSideMenu = () => {
    dispatch({type: 'UI.OPEN_SIDEBAR'});
  };

  const closeSideMenu = () => {
    dispatch({type: 'UI.CLOSE_SIDEBAR'});
  };

  const setIsAddingEntry = (bool: boolean) => {
    dispatch({type: 'UI.IS_ADDING_ENTRY', payload: bool});
  };

  const setIsDragging = (bool: boolean) => {
    dispatch({type: 'UI.IS_DRAGGING', payload: bool});
  };

  return (
    <UIContext.Provider value={{
      ...state,
      openSideMenu,
      closeSideMenu,
      setIsAddingEntry,
      setIsDragging,
    }}>
      {children}
    </UIContext.Provider>
  );
};
