import {createContext} from 'react';

interface ContextProps {
    sideMenuOpen: boolean;
    isAddingEntry: boolean;
    isDragging: boolean;
    setIsAddingEntry: (bool: boolean) => void;
    setIsDragging: (bool: boolean) => void;
    openSideMenu: () => void;
    closeSideMenu: () => void;
}

export const UIContext = createContext({} as ContextProps);
