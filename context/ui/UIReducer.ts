import {UIState} from './';

type UIActionType =
    | { type: 'UI.OPEN_SIDEBAR' }
    | { type: 'UI.CLOSE_SIDEBAR' }
    | { type: 'UI.IS_ADDING_ENTRY', payload: boolean }
    | { type: 'UI.IS_DRAGGING', payload: boolean }

export const UIReducer = (state:UIState, action: UIActionType): UIState => {
  switch (action.type) {
    case 'UI.OPEN_SIDEBAR':
      return {
        ...state,
        sideMenuOpen: true,
      };
    case 'UI.CLOSE_SIDEBAR':
      return {
        ...state,
        sideMenuOpen: false,
      };
    case 'UI.IS_ADDING_ENTRY':
      return {
        ...state,
        isAddingEntry: action.payload,
      };

    case 'UI.IS_DRAGGING':
      return {
        ...state,
        isDragging: action.payload,
      };

    default:
      return state;
  };
};
