import {useReducer, useEffect} from 'react';
import {Entry} from '../../interfaces';
import {EntriesContext, EntriesReducer} from './';
import {entriesApi} from '../../apis';
import {useSnackbar, OptionsObject, VariantType} from 'notistack';

export interface EntriesState {
  entries: Entry[],
}

const ENTRIES_INITIAL_STATE: EntriesState = {
  entries: [],
};

type Props = {
  children: React.ReactNode
}

export const EntriesProvider:React.FC<Props> = ({children}) => {
  const [state, dispatch] = useReducer(EntriesReducer, ENTRIES_INITIAL_STATE);
  const {enqueueSnackbar} = useSnackbar();
  const snackBarOptions = (variant: VariantType) : OptionsObject => ({
    variant,
    autoHideDuration: 1500,
    anchorOrigin: {
      vertical: 'top',
      horizontal: 'right',
    },
  });

  const addNewEntry = async (description: string, showSnackbar = false) => {
    try {
      const {data} = await entriesApi.post<Entry>('/', {description});
      if (showSnackbar) {
        enqueueSnackbar(
            'Entry successfully created',
            snackBarOptions('success'));
      }
      dispatch({type: 'ENTRY.ADD_ENTRY', payload: data});
    } catch (err) {
      enqueueSnackbar('Error creating the entry', snackBarOptions('error'));
      console.log(err);
    }
  };

  const updateEntry = async (
      {_id, description, status}: Entry,
      showSnackbar = false,
  ) => {
    try {
      const {data} = await entriesApi.put<Entry>(
          `/${_id}`,
          {description, status},
      );

      if (showSnackbar) {
        enqueueSnackbar(
            'Entry successfully updated',
            snackBarOptions('success'),
        );
      }
      dispatch({type: 'ENTRY.ENTRY_UPDATED', payload: data});
    } catch (err) {
      enqueueSnackbar('Error updating the entry', snackBarOptions('error'));
      console.log(err);
    }
  };

  const deleteEntry = async (entry: Entry, showSnackbar = false) => {
    try {
      await entriesApi.delete<Entry>(`/${entry._id}`);

      if (showSnackbar) {
        enqueueSnackbar(
            'Entry successfully deleted',
            snackBarOptions('success'),
        );
      }

      dispatch({type: 'ENTRY.DELETE_ENTRY', payload: entry});
    } catch (err) {
      enqueueSnackbar('Error deleting the entry', snackBarOptions('error'));
      console.log(err);
    }
  };

  const refreshEntries = async () => {
    const {data} = await entriesApi.get<Entry[]>('/');
    dispatch({type: 'ENTRY.REFRESH_ENTRIES', payload: data});
  };

  useEffect(() => {
    refreshEntries();
  }, []);


  return (
    <EntriesContext.Provider value={{
      ...state,
      addNewEntry,
      updateEntry,
      deleteEntry,
    }}>
      {children}
    </EntriesContext.Provider>
  );
};

