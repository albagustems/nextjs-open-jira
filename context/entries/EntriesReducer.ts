import {Entry} from '../../interfaces';
import {EntriesState} from './';

type EntriesActionType =
  | {type: 'ENTRY.ADD_ENTRY', payload: Entry}
  | {type: 'ENTRY.ENTRY_UPDATED', payload: Entry}
  | {type: 'ENTRY.REFRESH_ENTRIES', payload: Entry[]}
  | {type: 'ENTRY.DELETE_ENTRY', payload: Entry}

export const EntriesReducer = (
    state:EntriesState,
    action: EntriesActionType): EntriesState => {
  switch (action.type) {
    case 'ENTRY.ADD_ENTRY':
      return {
        ...state,
        entries: [...state.entries, action.payload],
      };

    case 'ENTRY.ENTRY_UPDATED':
      return {
        ...state,
        entries: state.entries.map((entry) => {
          if (entry._id === action.payload._id) {
            entry.status = action.payload.status;
            entry.description = action.payload.description;
          }

          return entry;
        }),
      };

    case 'ENTRY.REFRESH_ENTRIES':
      return {
        ...state,
        entries: [...action.payload],
      };

    case 'ENTRY.DELETE_ENTRY':
      return {
        ...state,
        entries: state.entries.filter((entry) => {
          return entry._id !== action.payload._id;
        }),
      };

    default:
      return state;
  }
};
