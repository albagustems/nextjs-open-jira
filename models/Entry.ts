import mongoose, {Model, Schema} from 'mongoose';
import {v4 as uuidv4} from 'uuid';
import {Entry} from '../interfaces';

export interface EntryInterface extends Entry {}

const entrySchema = new Schema({
  _id: {type: String, default: uuidv4},
  description: {type: String, required: true},
  status: {
    type: String,
    enum: {
      values: ['to-do', 'in-progress', 'under-review', 'done'],
      message: '{VALUE} state not allowed',
    },
    default: 'to-do',
  },
  createdAt: {type: Number, default: Date.now()},
});

const EntryModel: Model<EntryInterface> =
    mongoose.models.Entry || mongoose.model('Entry', entrySchema);

export default EntryModel;
